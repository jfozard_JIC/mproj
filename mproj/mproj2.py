
import numpy as np
import SimpleITK as sitk
import matplotlib.pyplot as plt
import scipy.ndimage as nd

from tifffile import TiffWriter, TiffFile, imsave
import sys
from PIL import Image

MIN_BRIGHTNESS = 10
GAUSSIAN_RADIUS = 1
MASK_R = 5
TR_SCALE = 1.5

def load_tiff(fn):
    with TiffFile(fn) as tiff:
        data = tiff[0].asarray(colormapped=False)
        print data.shape
        data = np.squeeze(data)
        x_sp =  tiff[0].tags['x_resolution'].value
        y_sp =  tiff[0].tags['y_resolution'].value
        z_sp = tiff[0].imagej_tags.get('spacing', 1.0)
    

    x_sp = float(x_sp[0])/x_sp[1]
    y_sp = float(y_sp[0])/y_sp[1]
    return data, (x_sp, y_sp, z_sp)

def write_tiff(fn, A, spacing):
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(spacing[0], spacing[1]),
                metadata={'spacing': spacing[2], 'unit': 'um'})  


def main():
    # Assume / treat image as if voxels are cubic
    im, spacing = load_tiff(sys.argv[1])
    print im.shape, spacing, im.dtype

#    im = im[:,500:600,500:600]

#    write_tiff('crop.tif', im, spacing)


    # Step 1 and 2
    """After acquisition, the images were further preprocessed to
    reduce background noise. This consisted first of the appli-
    cation of a small Gaussian filter of radius between 1 and 5
    pixels to smooth images, in particular to reduce the impact
    of isolated bright or dark dots. In addition, a threshold was
    applied, removing all the pixels with a brightness less than
    an empirically defined constant (we used 10 for images with
    256 grey levels). This was followed by a linear contrast
    optimization to ensure that grey levels ranged from 0 to 255."""

    print "start preprocess"
    blur_im = nd.gaussian_filter(im, GAUSSIAN_RADIUS).astype(float)
    blur_im = blur_im - MIN_BRIGHTNESS
    blur_im = (255.0*(np.clip(blur_im/np.max(blur_im), 0.0, 1.0))).astype(np.uint8)
    print "end preprocess"
    
    write_tiff('preprocess.tif', blur_im, spacing)
    # Step 3
    # Construction of transparency mask
    
    # Closure with a diamond/octahedral shaped structuring element - indicated by
    # the SI and the diamond shape of the flat regions in Figure 2
    
    # Generate structuring element (size (2*MASK_R+1)**3)
    r = MASK_R
    i, j, k =np.ogrid[-r:r+1,-r:r+1,-r:r+1]
    selem = (np.abs(i + j + k) <= r)

    # Grey level morphological closure from ndimage
    print "start closing ITK"
    itk_im = sitk.GetImageFromArray(im.astype(np.int16))
    for u in range(MASK_R):
        print u
        itk_im = sitk.GrayscaleDilate(itk_im, 1)#MASK_R)
    for u in range(MASK_R):
        print u
        itk_im = sitk.GrayscaleErode(itk_im, 1)#MASK_R)
#   itk_im = sitk.GrayscaleMorphologicalClosing(itk_im, MASK_R)
    transparency_mask = sitk.GetArrayFromImage(itk_im)

#    transparency_mask = nd.grey_closing(im, structure=selem)
    print "end closing ITK"

    

    # Watershed segmentation - use SimpleITK implementation
    # May be better to use another one for production to 
    # minimze dependencies

    print "start watershed"
    itk_im = sitk.GetImageFromArray(blur_im.astype(np.int16))
    itk_im = sitk.MorphologicalWatershed(itk_im, markWatershedLine=False)
    seg_im = sitk.GetArrayFromImage(itk_im)
    print "end watershed"

    
    # Find minimum for each labelled region in seg_im
    print "start remove bg watershed labels"
    l = np.unique(seg_im)
    min_vals = nd.minimum(transparency_mask, labels=seg_im, index=l)
    
    write_tiff('trans_orig.tif', transparency_mask, spacing)
    
    
    # Set closed_im to zero for all labelled regions in seg_im
    # whose minimum value is zero
    removed_vals = l[min_vals==0]
    print removed_vals
    for u in removed_vals:
        transparency_mask[seg_im==u] = 0
        seg_im[seg_im==u] = 0
    print "end remove bg watershed labels"


    write_tiff(sys.argv[2], transparency_mask, spacing)
    write_tiff('seg.tif', seg_im.astype(np.uint8), spacing)

    # Make composite image from the bottom-up

    print "start composite"
    composite = np.zeros((im.shape[1], im.shape[2]), dtype=float)
    for i in range(im.shape[0]-1, -1, -1):
        alpha = np.clip(transparency_mask[i,:,:]/255.0*TR_SCALE, 0, 1)
        composite = alpha*im[i,:,:]+(1-alpha)*composite
    print "end composite"
    composite = composite.astype(np.uint8)

    out_im = Image.fromarray(composite)
    out_im.save(sys.argv[3])

    



if __name__=="__main__":
    main()
