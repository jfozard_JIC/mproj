#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage as nd
from math import floor
from PIL import Image

"""
Reimplementation of merryproj "Compute masks without watersheds".
Note that output differs slightly from original merryproj; likely due to rounding
differences.
Also output is as a single channel png, rather than a RGB PNG

Usage:
 python mproj_slices_simple.py input_stack.tif output_projection.png

"""

from tifffile import TiffWriter, TiffFile, imsave # pip install tifffile; only required for tiff io.

import sys

def load_tiff(fn):
    with TiffFile(fn) as tiff:
        data = tiff.asarray()
        print data.shape
        data = np.squeeze(data)
        x_sp = tiff.pages[0].tags['XResolution'].value           
        y_sp = tiff.pages[0].tags['YResolution'].value           
        z_sp = tiff.imagej_metadata.get('spacing', 1.0)
    

    x_sp = float(x_sp[1])/x_sp[0]
    y_sp = float(y_sp[1])/y_sp[0]
    return data, (x_sp, y_sp, z_sp)

def write_tiff(fn, A, spacing):
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(spacing[0], spacing[1]),
                metadata={'spacing': spacing[2], 'unit': 'um'})  


def mproj(im, mask_r=19):
    """
    Args:
       im (numpy.ndarray) : 3d uint8 numpy array (z,y,x) containing confocal stack
                            larger z -> deeper into stack.
       mask_r : Size of the structuring element used for gray-level dilation
                One less than the value in merryproj; 
                set via dialog box in merryproj gui "Enter the maximum size of a cell"
                default value there is 20

    Returns:
       numpy.ndarray : 2d uint8 numpy array containing projected stack
    """

    
    # Step 1 and 2
    """After acquisition, the images were further preprocessed to
    reduce background noise. This consisted first of the appli-
    cation of a small Gaussian filter of radius between 1 and 5
    pixels to smooth images, in particular to reduce the impact
    of isolated bright or dark dots. In addition, a threshold was
    applied, removing all the pixels with a brightness less than
    an empirically defined constant (we used 10 for images with
    256 grey levels). This was followed by a linear contrast
    optimization to ensure that grey levels ranged from 0 to 255."""

    def rescale(im):
        return (255.0*(np.clip(im.astype(float)/np.max(im), 0.0, 1.0))).astype(np.uint8)
    
    # Generate structuring element (size (2*MASK_R+1)**3)
    r = mask_r
    i, j =np.ogrid[-r:r+1,-r:r+1]
    selem = (np.abs(i) + np.abs(j) <= r).astype(np.uint8)

    # Grey level morphological closure from ndimage
    transparency_mask = np.zeros_like(im)
    for i in range(im.shape[0]-1):
        u = im[i,:,:]
        # Pierre's grey closing code is subtly different from this, but this seems to agree most
        # of the time
        # His code quantizes the image to 16 levels, uses (unchecked) algo with
        # one forward and one backward sweep to compute distance transform with manhattan metric
        # Then metric at each level is thresholded and used to recover image
        u = rescale(u)
        u = (u/16).astype(np.uint8)
        u = nd.grey_closing(u, footprint=selem)
        u = (u*16).astype(np.uint8)
        u = rescale(u).astype(np.uint8)
        # Pierre's "Gaussian" operator (ndimage.uniform_filter gives slightly different results)
        u = (nd.convolve(u.astype(float),np.ones((3,3)))/9.0).astype(np.uint8)
        transparency_mask[i,:,:] = u
    transparency_mask[-1,:,:] = 255

    # Make composite image from the bottom-up

    composite = np.zeros((im.shape[1], im.shape[2]), dtype=float)
    for i in range(im.shape[0]-1, -1, -1):
        u = rescale(im[i,:,:]) # No preprocessing applied directly in merryproj but images
                               # rescaled to be from 0-255 through conversion to/from float
        alpha = (transparency_mask[i,:,:] & 255) /255.0 
        composite = np.floor(alpha*u+(1-alpha)*composite).astype(np.uint8)

    return composite


def main():
    im, spacing = load_tiff(sys.argv[1])
    composite = mproj(im)
    
    out_im = Image.fromarray(composite)
    out_im.save(sys.argv[2])



if __name__=="__main__":
    main()
